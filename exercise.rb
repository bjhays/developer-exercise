class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    upper_replace = "Marklar"
    lower_replace = "marklar"

    clean_str = str.gsub(/[^0-9A-Za-z ]/, '')
    words = clean_str.split
    words.each do |word|
      if word.length > 4 
        if /[[:upper:]]/.match(word.each_char.first)
          str.gsub! word, upper_replace
        else
          str.gsub! word, lower_replace
        end
      end
    end
    return str
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    sum=0
    fibs=(nth-1).times.each_with_object([0,1]) do |num, obj| 
      obj << obj[-2] + obj[-1]
    end
    fibs.each do |num|
      if num%2==0
        sum+=num
      end 
    end
    return sum
  end

end
